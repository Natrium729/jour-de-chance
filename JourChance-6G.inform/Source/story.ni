"Jour de Chance" by Natrium729

[
Pour faire bref :
Ce code source est libre ; vous pouvez le redistribuer ou le modifier suivant les termes de la « GNU General Public License » telle que publiée par la Free Software Foundation : soit la version 3 de cette licence, soit toute version ultérieure.

Ce programme est distribué dans l'espoir qu'il vous sera utile, mais SANS AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ ni d'ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale Publique GNU pour plus de détails.

Pour faire long, consultez : http://www.gnu.org/licenses
]


Volume 1 - Initialisation

Book 1 - Extensions

Part 1 - French

Include French by Eric Forgeot. Use French 1PSPr Language.


Part 2 - Screen effect

Include Basic Screen Effects by Emily Short.


Book 2 - Options

Use full-length room descriptions.
The maximum score is 4.


Volume 2 - Jeu

Part 1 - Introduction

The pièce is female thing. The player carries the pièce.
Understand "piece" as the pièce.
The description of the pièce is "La pièce. Celle avec laquelle je vais gagner. Je pense.".

Instead of dropping the pièce, say "Jamais ! Elle est mon unique chance !".

When play begins:
	say "Enfin. La fête foraine.";
	wait for any key;
	say "Devant le stand de la loterie.";
	wait for any key;
	say "Avec une pièce dans la poche.";
	wait for any key;
	say "Mon unique chance de gagner la super console de jeux [italic type]Yoo.[roman type]";
	wait for any key;
	say "[line break]Mais avec ma chance légendaire, je n'ai… eh bien… aucune chance justement. Pourrais-je la remporter ?";
	wait for any key.


Part 2 - Stand de la loterie

The Stand is a room. "« Par ici ! Venez tenter votre chance de gagner la toute nouvelle [italic type]Yoo[roman type] ! » crie le forain. Tenter ma chance ? Ma malchance, oui ! Pourtant, je dois réussir.".
The printed name of the stand is "Devant le stand de la loterie".

After looking when the score is 4 and the location is the stand, say "Je pense que cette fois-ci, ma chance sera suffisante pour gagner la console !".


Chapter 1 - Forain

The forain is a man in the stand.
The indefinite article of the forain is "le".
The description of the forain is "Un gars costaud, qui s[']égosille à appeler les passants. Il tient le stand de la loterie."

Instead of talking to the forain:
	say "« Toi ! Tu veux jouer n'est-ce pas ? Alors donne-moi une pièce et voyons si tu réussis ! »[line break]".
	
Instead of giving the pièce to the forain:
	say "Allez, je donne la pièce au forain et tire un numéro. La roulette tourne. Une goutte de sueur coule le long de ma tempe. Et là…[line break]";
	wait for any key;
	if the score < 4
	begin;
		say "Nooooooooooooooooooooooooooooooon !!!!!";
		wait for any key;
		end the story saying "J'ai perdu !";
	otherwise;
		say "Ouiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii !!!!!";
		wait for any key;
		end the story finally saying "J'ai gagné !";
	end if.


Chapter 2 - Pancarte

The pancarte is a female fixed in place thing in the stand.
The description of the pancarte is "On peut lire :[line break]« ^ Hall des miroirs ^[line break]< Train fantôme (fermé pour réparations) <[line break]V Carrousel V »".
Understand "panneau" as the pancarte.

Instead of reading the pancarte, try examining the pancarte.


Part 3 - Hall des miroirs

The Hall des miroirs is north from the stand. "Me voici à l'entrée du Hall des miroirs. Heureusement, c'est gratuit. Mais ai-je vraiment envie de me perdre dans ce labyrinthe ?[line break]L'attraction commence au nord.".
The printed name of the hall des miroirs is "Dans le Hall des miroirs".


Chapter 1 - Panneau

The panneau is a fixed in place thing in the Hall des miroirs.
The printed name of the panneau is "panneau « Interdit aux chiens »".
The description of the panneau is "Un panneau indiquant que nos amis à quatre pattes ne sont pas les bienvenus.".


Chapter 2 - Papier

The papier is a thing in the hall des miroirs. "Un morceau de papier traîne par terre."
The printed name of the papier is "morceau de papier".
Understand "morceau de papier" as the papier.
The description of the papier is "Un dessin a été griffonné au dos de cette publicité. On dirait une carte :[paragraph break][fixed letter spacing] O--O    O[line break] |  |   /[line break] O  O--O[line break] |[line break]HALL[variable letter spacing]".


Part 4 - Labyrinthe

[Chemin : n n e s e ne]

A mirrorhall is a kind of room. The printed name of a mirrorhall is always "Au milieu de miroirs". The description of a mirrorhall is always "Des miroirs, encore des miroirs, toujours des miroirs. Mais où est donc la sortie ?".

Instead of going nowhere from a mirrorhall:
	say "Boum ! Dzliing ! Je viens de heurter l'un de ces maudits miroirs. Résultat : de grosses fissures…[line break]";
	wait for any key;
	say "Nooon !!! Sept ans de malheur !!! Je n'aurai jamais ma console !";
	wait for any key;
	end the story saying "Je suis perdu !".

Miroir1 is a mirrorhall. Miroir1 is north from the hall des miroirs.
Miroir2 is a mirrorhall. Miroir2 is north from the miroir1.
Miroir3 is a mirrorhall. Miroir3 is east from the miroir2.
Miroir4 is a mirrorhall. Miroir4 is south from the miroir3.
Miroir5 is a mirrorhall. Miroir5 is east from the miroir4.
Miroir6 is a mirrorhall. Miroir6 is northeast from the miroir5.

After going to miroir6 for the first time:
	say "Splouich ! Comme j'entre dans cette salle, j'entends un bruit répugnant. Et m… zut ! j'ai marché dans une crotte de chien. Ils étaient pourtant interdits !";
	wait for any key;
	say "Attends une minute… Du pied gauche ! Cela porte chance !";
	wait for any key;
	increase the score by one;
	continue the action.


Chapter 1 - Miroirs

Some miroirs-sc are backdrops.
The printed name is "miroirs".
Understand "miroir" or "miroirs" as the miroirs-sc.
The miroirs-sc are in Miroir1, Miroir2, Miroir3, Miroir4, Miroir5 and Miroir6.
The description is "Tiens, c'est moi ici. Et ici aussi. Et encore ici. Et un autre ici. Je me vois de tous côtés, mais où est la sortie ?".


Chapter 2 - Crotte

The crotte is a female thing in miroir6.
Understand "excrement", "excrements", "caca" or "merde" as the crotte.
The description is "Mieux vaut regarder autre chose.".

Instead of taking the crotte, say "Beuârk ! Et puis quoi encore ?".
Instead of smelling miroir6, say "Il y a comme une odeur…[line break]".
Instead of smelling the crotte, say "Je pense que j'ai des occupations plus agréables que cela.".


Part 5 - Train fantôme

The train fantôme is west from the stand. "Je suis devant le comptoir, à l'entrée du Train Fantôme. Je n'ai jamais aimé cette attraction, elle donne la chair de poule. De toute façon, c'est fermé."
The printed name of the train fantôme is "Devant le comptoir du Train Fantôme".


Chapter 1 - Peintre

The peintre is a man in the train fantôme. "Un peintre repeint un mur en violet sombre."
The description of the peintre is "Un peintre tout ce qu'il y a de plus normal.".

Instead of talking to the peintre, say "« [one of]Salut petit. Désolé, l'attraction est fermée pour le moment. En revanche, savais-tu que le Wolpertinger censé effrayer les gens était réellement un lapin empaillé, avec des bois en plastique ? Quoi, tu veux le voir ? Mmm… Oui, pourquoi pas ? Allez, je te laisse passer. Fais vite ! L'entrée est au sud. »[or]Alors, tu l'as vu ? »[stopping][line break]".


Part 6 - Couloir

The couloir is south from the train fantôme. "Un endroit sinistre. Plus vite je serais sorti, mieux cela vaudra. Une chance que le train ne fonctionne pas.[line break]Les rails partent vers le sud.".
The printed name of the couloir is "Le long d'un couloir lugubre".


Part 7 - Caverne

The caverne is south from the couloir. "Une salle aux murs imitant la roche. Cela fait drôlement peur. Au loin, je peux voir une paire d'yeux rouges… le Wolpertinger !".
The printed name of the caverne is "Dans une sombre caverne".

Before going to the caverne:
	if the player is carrying the switched on lampe torche, say "Il y a une échelle devant moi. Normal, l'endroit est en réparation. Je la contourne afin d'avancer.";
	if the player is not carrying the lampe torche or the lampe torche is switched off
	begin;
		say "Dans le noir, je me cogne contre quelque chose de métallique. Le tâtant, je me rends compte qu'il s'agit d'une échelle, et que je suis passé dessous. Nooon, cela porte malheur, je n'aurai jamais ma console !!!";
		wait for any key;
		end the story saying "Je suis perdu !";
	end if.


Chapter 1 - Wolpertinger

The Wolpertinger is a fixed in place animal in the caverne.
Understand "lapin" or "patte" as the Wolpertinger.
The indefinite article of the Wolpertinger is "le".
The description of the Wolpertinger is "Un effrayant lapin, avec des bois de cerf. Il fiche vraiment la trouille. Qui a eu l'idée de le mettre ici ?".

Before kissing the Wolpertinger for the first time:
	say "Surmontant ma peur, j'embrasse la patte du Wolpertinger. Ça y est, la chance est avec moi !";
	increase the score by 1;
	stop the action.


Part 8 - Carrousel

The carrousel is south from the stand. "Un antique manège pour enfants. Je venais souvent étant petit. Maintenant, je suis trop grand pour ces chevaux à vilebrequin.[line break]Il y a une boutique à l'est.".
The printed name of the carrousel is "Devant le carrousel".


Chapter 1 - Manège

The manège is scenery in the carrousel. Understand "carrousel", "manege", "cheval" or "chevaux" as the manège.
The description of the manège is "Une vieille attraction, toujours d'actualité : de nombreux enfants viennent toujours profiter de ce manège indémodable. Les chevaux portent même de vrais fers !".


Chapter 2 - Fer

The fer is a undescribed thing in the carrousel. The printed name of the fer is "fer à cheval".
The description of the fer is "Un fer à cheval ordinaire peut-être, mais un fer à cheval porte chance ! C'est juste ce qu'il me faut !".

After taking the fer:
	say "Profitant de l'arrêt du carrousel et d'un moment d'inattention du forain, j'arrache l'un des fers. Cela porte chance !";
	increase the score by one.

Instead of dropping the fer, say "Je ne me séparerai pas d'un porte-bonheur pareil !".


Part 9 - Boutique

The boutique is east from the carrousel. "Une petite échoppe tenue par un petit commerçant. Il vend toute sorte de choses. Malheureusement, je garde ma pièce pour la loterie. Peut-être que je pourrais… Non, quand même pas.".
The printed name of the boutique is "Devant une petite boutique".


Chapter 1 - Marchand

The marchand is an undescribed man in the boutique.
The description is "Un petit homme, vendant toutes sortes de choses. Malheureusement, je n'ai pas assez d'argent.".
The marchand has a truth state called attention. The attention of the marchand is false.

Instead of talking to the marchand, say "« Alors, quelque chose t'intéresse ? »[line break]".

Instead of giving the pièce to the marchand, say "« Désolé, petit. Je n'ai rien à ce prix-là. »[line break]".

Every turn:
	now the attention of the marchand is a random truth state;
	if the location is boutique, say "Le marchand [if attention of the marchand is false][one of]regarde derrière lui, cherchant un objet à mettre en vente[or]est occupé à discuter avec un client[or]se baisse, cherchant une pièce qu'il vient de faire tomber[at random][else][one of]surveille avec attention ses marchandises[or]est attentif à vos moindres mouvements[or]n'est pas prêt de se laisser piquer ses affaires[at random][end if].".


Chapter 2 - Trèfle

The trèfle is a thing in the boutique. "Un trèfle est posé sur l[']étal.".
Understand "trefle" as the trèfle.
The description of the trèfle is "Ce n'est pas n'importe quel trèfle : il s'agit d'un rarissime trèfle à quatre feuilles !".

Instead of dropping the trèfle, say "Je ne me séparerai pas d'un porte-bonheur pareil !".
Instead of eating the trèfle, say "Je n'allais tout de même pas le manger !".

Taking the trèfle is steeling action.

Carry out taking the trèfle:
	increase the score by 1.


Chapter 3 - Torche

The lampe torche is a female switched off device in the boutique. "Une lampe torche est posé sur l[']étal.".
Understand "lampe" or "torche" as the lampe torche.
The description of the lampe torche is "Une puissante lampe torche, capable de dissiper les ténèbres les plus sombres.".

Instead of dropping the lampe torche, say "Je pense que j'en aurai besoin.".

Taking the lampe torche is steeling action.


Chapter 4 - Prendre la torche ou le trèfle

After steeling action:
	if the attention of the marchand is false
	begin;
		say "Je m'approche discrètement de la boutique et subtilise l'objet, sans que le marchand ne me voie.";
	else;
		say "Le marchand m'a vu et me crie dessus. En guise de paiement, il me prend la pièce, même si ce n'est pas suffisant. Je ne pourrais jamais avoir ma console !";
		wait for any key;
		end the story saying "Je suis perdu !";
	end if.


Volume 3 - Release
	
The story headline is "Un loterie interactive".
The story genre is "Comédie".
The release number is 2.
The story description is "Une pièce dans la poche, vous avez décidé d'aller à la loterie afin d'essayer de gagner la toute dernière console de jeux. Cependant, vous connaissez votre chance et il vous sera très difficile de la remporter. À moins que vous trouviez ces objets dont on dit qu'ils portent bonheur...".
The story creation year is 2011.

Release along with cover art, a solution, the source text and a file of "Changelog" called "JDC-changelog.txt".


Book 1 - Commandes

Part 1 - Aide

After printing the banner text, say "[first time]Tapez « aide » pour obtenir plus d'informations sur le jeu ou si vous jouez à une fiction interactive pour la première fois.[only]".

Understand "aide", "infos", "info", "informations" or "information" as a mistake ("Ce jeu est une fiction interactive ; il se joue en tapant des commandes à l'infinitif telles que « voir », « prendre [italic type]objet[roman type] », etc. (pour plus d'informations, consultez [italic type]www.ifiction.free.fr[roman type]).[line break]Tapez « licence » pour l'obtenir et « auteur » pour en savoir plus sur l'auteur.").


Part 2 - Licence

Understand "licence" or "license" as a mistake ("[bold type]Pour faire bref :[line break][roman type]Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes de la « GNU General Public License » telle que publiée par la Free Software Foundation : soit la version 3 de cette licence, soit toute version ultérieure.[line break]Ce programme est distribué dans l'espoir qu'il vous sera utile, mais SANS AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ ni d'ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale Publique GNU pour plus de détails.[paragraph break][bold type]Pour faire long, [roman type]consultez : [italic type]http://www.gnu.org/licenses[roman type][paragraph break]Les images de la couverture sont sous la licence Creative Commons BY-SA ([italic type]www.creativecommons.org/licenses/by-sa/3.0/deed.fr[roman type]). Le trèfle est de Phyzome, le fer de Fonzy et le fond de David Monniaux. Les images ont été trouvées sur Wikimedia.").


Part 3 - Auteur

Understand "credits", "credit", "auteur" or "auteurs" as a mistake ("C'est moi, Natrium729 ! Vous ne savez pas la chance de que vous avez en me connaissant ! Mais si un jour elle vous fait défaut, n'hésitez pas à me contacter à l'adresse [italic type]natrium729@gmail.com[roman type], ou à venir directement commenter sur mon site, [italic type]http://ulukos.com[roman type] ! Au passage, je remercie mes veinard de bêta-testeurs, qui se reconnaîtront (si par hasard vous vous reconnaissez et désirer avoir votre nom d'affiché, dites-le moi, on pourrait s'arranger).").