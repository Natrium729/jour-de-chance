"Jour de chance" by Nathanaël Marion (in French)

Volume 1 - Initialisations

Use unabbreviated object names.

Book 1 - Les extensions

Include Basic Screen Effects by Emily Short.

Include Experimental French Features by Nathanael Marion.

Include (- 
language French

<control-structure-phrase> ::=
	/a/ si ... est début |
	/b/ si ... est |
	/c/ si ... |
	/c/ à moins que |
	/d/ répéter/parcourir ... |
	/e/ tant que ... |
	/f/ sinon |
	/g/ sinon si ... |
	/g/ sinon à moins que |
	/h/ -- sinon |
	/i/ -- ... |
-)  in the Preform grammar.

Book 2 - Le score

Use scoring.
Le maximum score est 4.

The notify score changes rule is not listed in any rulebook.

When play begins:
	maintenant le right hand status line est "Chance : [score]".

Book 3 - Les nouvelles actions

Part 1 - L'action de parler

Talking to is an action applying to one visible thing.
Understand "talk to [something]", "speak to [something]", "parler a/au/aux/avec/-- [something]" or "questionner [something]" as talking to.
Understand the command "discuter" as "parler".

Check talking to:
	si the noun n'est pas une personne:
		dire "Je ne peux pas parler [au noun][_]!" instead.

Check talking to:
	si le noun est le player:
		dire "Je me parle à moi-même pendant un moment." instead.

Report talking to:
	say "Je n'ai rien à dire.".

Rule for clarifying the parser's choice of something (called item) while talking to:
	say "([au item])[command clarification break]".

Does the player mean talking to a person: it is very likely.

Volume 2 - Le jeu

Part 1 - L'introduction

La pièce est une chose. Le player porte la pièce.
La description est "La pièce. Celle avec laquelle je vais gagner. Je pense.".

Comprendre "piece" comme la pièce.

Instead of dropping la pièce, dire "Jamais[_]! Elle est mon unique chance[_]!".

When play begins:
	maintenant le story viewpoint est la première personne du singulier;
	dire "Enfin. La fête foraine.";
	attendre une touche;
	dire "Devant le stand de la loterie.";
	attendre une touche;
	dire "Avec une pièce dans la poche.";
	attendre une touche;
	dire "Mon unique chance de gagner la super console de jeux [italique]Yoo.[romain]";
	attendre une touche;
	dire "[line break]Mais avec ma chance légendaire, je n'ai… eh bien… aucune chance justement. Pourrais-je la remporter[_]?";
	attendre une touche.

Part 2 - le stand de la loterie

Le stand est un endroit. "«[_]Par ici[_]! Venez tenter votre chance de gagner la toute nouvelle [italique]Yoo[romain][_]![_]» crie le forain. Tenter ma chance[_]? Ma malchance, oui[_]! Pourtant, je dois réussir.".
Le printed name est "Devant le stand de la loterie".

After looking when le score est 4 et la location est le stand:
	dire "Je pense que cette fois-ci, ma chance sera suffisante pour gagner la console[_]!".

Chapter 1 - Le forain

Le forain est un homme dans le stand.
L' indefinite article du forain est "le".
La description est "Un gars costaud, qui s'égosille à appeler les passants. Il tient le stand de la loterie.".

Comprendre "gars" comme le forain.

Instead of talking to le forain:
	dire "«[_]Toi[_]! Tu veux jouer n'est-ce pas[_]? Alors donne-moi une pièce et voyons si tu réussis[_]![_]»[à la ligne]".
	
Instead of giving la pièce to le forain:
	dire "Allez, je donne la pièce au forain et tire un numéro. La roulette tourne. Une goutte de sueur coule le long de ma tempe. Et là…[à la ligne]";
	attendre une touche;
	si le score est inférieur à 4:
		dire "Nooooooooooooooooooooooooooooooon[_]!!!!!";
		attendre une touche;
		fin de l'histoire en disant "J'ai perdu[_]!";
	sinon:
		dire "Ouiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii[_]!!!!!";
		attendre une touche;
		fin définitive de l'histoire en disant "J'ai gagné[_]!".

Chapter 2 - La pancarte

La pancarte est chose fixée sur place dans the stand.
La description est "On peut lire :[saut de paragraphe][italique]^ Hall des miroirs[_]: nord[à la ligne]< Train fantôme (fermé pour réparations)[_]: ouest [à la ligne]V Carrousel[_]: sud[romain]".

Comprendre "panneau" comme la pancarte.

Part 3 - Le Hall des miroirs

Le Hall des miroirs est au nord du stand. "Me voici à l'entrée du Hall des miroirs. Heureusement, c'est gratuit. Mais ai-je vraiment envie de me perdre dans ce labyrinthe[_]?[à la ligne]L'attraction commence au nord.".
Le printed name est "Dans le Hall des miroirs".

Chapter 1 - Le panneau

Le panneau est une chose fixée sur place dans les Hall des miroirs.
Le printed name est "panneau «[_]Interdit aux chiens[_]»".
La description est "Un panneau indiquant que nos amis à quatre pattes ne sont pas les bienvenus.".

Comprendre "interdit", "aux" ou "chiens" comme le panneau.

Chapter 2 - Le papier

Le morceau de papier est une chose dans le Hall des miroirs. "Un morceau de papier traîne par terre."
La description est "Un dessin a été griffonné au dos de cette publicité. On dirait une carte :[saut de paragraphe][largeur fixe] O[--][--]O    O[à la ligne] |  |   /[à la ligne] O  O[--][--]O[à la ligne] |[à la ligne]HALL[largeur variable]".

[La carte :

 O——O    O
 |  |   /
 O  O——O
 |
HALL
]

Part 4 - Le labyrinthe

[Chemin : nord, nord, est, sud, est, nord-est.]

A maze is a kind of endroit. A maze translates into French as un labyrinthe.
Le printed name d' un labyrinthe est toujours "Au milieu de miroirs".
La description d' un labyrinthe est toujours "Des miroirs, encore des miroirs, toujours des miroirs. Mais où est donc la sortie[_]?".

Instead of going nowhere from un labyrinthe:
	dire "Boum[_]! Dzliing[_]! Je viens de heurter l'un de ces maudits miroirs. Résultat[_]: de grosses fissures…[à la ligne]";
	attendre une touche;
	dire "Nooon[_]!!! Sept ans de malheur[_]!!! Je n'aurai jamais ma console[_]!";
	attendre une touche;
	fin de l'histoire en disant "Je suis perdu[_]!".

Miroir1 est un labyrinthe. Miroir1 est au nord du Hall des miroirs.
Miroir2 est un labyrinthe. Miroir2 est au nord de miroir1.
Miroir3 est un labyrinthe. Miroir3 est à l'est de miroir2.
Miroir4 est un labyrinthe. Miroir4 est au sud de miroir3.
Miroir5 est un labyrinthe. Miroir5 est à l'est de miroir4.
Miroir6 est un labyrinthe. Miroir6 est au nord-est de miroir5.

After going to miroir6 pour la première fois:
	dire "Splouich[_]! Comme j'entre dans cette salle, j'entends un bruit répugnant. Et m… zut ! j'ai marché dans une crotte de chien. Ils étaient pourtant interdits[_]!";
	attendre une touche;
	dire "Attends une minute… Du pied gauche[_]! Cela porte [italique]chance[romain][_]!";
	attendre une touche;
	augmenter le score de 1;
	continuer l'action.

Chapter 1 - Les miroirs

Les miroirs-décors sont une toile de fond privately-named.
Le printed name est "miroirs".
Les miroirs-décors sont dans Miroir1, Miroir2, Miroir3, Miroir4, Miroir5 et Miroir6.
La description est "Tiens, c'est moi ici. Et ici aussi. Et encore ici. Et un autre ici. Je me vois de tous côtés, mais où est la sortie[_]?".

Comprendre "miroir" or "miroirs" comme les miroirs-décors.

Chapter 2 - La crotte

La crotte est une chose dans miroir6.
La description est "Mieux vaut regarder autre chose.".

Comprendre "excrement", "excrements", "caca" ou "merde" comme la crotte.

Instead of taking la crotte:
	dire "Beuârk[_]! Et puis quoi encore[_]?".

Instead of smelling miroir6:
	dire "Il y a comme une odeur…[line break]".

Instead of smelling the crotte:
	dire "Je pense que j'ai des occupations plus agréables que cela.".

Part 5 - Le train fantôme

Le train fantôme est à l'ouest du stand. "Je suis devant le comptoir, à l'entrée du Train Fantôme. Je n'ai jamais aimé cette attraction, elle donne la chair de poule. De toute façon, c'est fermé."
Le printed name est "En face du comptoir du Train Fantôme".

Chapter 1 - Le peintre

Le peintre est un homme dans le train fantôme. "Un peintre repeint un mur en violet sombre."
La description est "Un peintre tout ce qu'il y a de plus normal.".

Instead of talking to le peintre:
	dire "« [parmi]Salut petit. Désolé, l'attraction est fermée pour le moment. En revanche, savais-tu que le Wolpertinger censé effrayer les gens était réellement un lapin empaillé, avec des bois en plastique[_]? Quoi, tu veux le voir[_]? Mmm… Oui, pourquoi pas[_]? Allez, je te laisse passer. Fais vite[_]! L'entrée est au sud.[_]»[ou]Alors, tu l'as vu[_]? »[stoppant][à la ligne]".

Part 6 - Le couloir

Le couloir est au sud du train fantôme. "Un endroit sombre et sinistre. Plus vite je serais sorti, mieux cela vaudra. Une chance que le train ne fonctionne pas.[à la ligne]Les rails partent vers le sud.".
Le printed name est "Le long d'un couloir lugubre".

Part 7 - La caverne

La caverne est au sud du couloir. "Une salle aux murs imitant la roche. Cela fait drôlement peur. Au loin, je peux voir une paire d'yeux rouges… le Wolpertinger[_]!".
Le printed name est "Dans une sombre caverne".

Before going to la caverne:
	si le player porte la allumée lampe torche:
		dire "Il y a une échelle devant moi. Normal, l'endroit est en réparation. Je la contourne afin d'avancer.";
	sinon si le player ne porte pas la lampe torche ou la lampe torche est éteinte:
		dire "Dans le noir, je me cogne contre quelque chose de métallique. Le tâtant, je me rends compte qu'il s'agit d'une échelle, et que je suis passé dessous. Nooon, cela porte malheur, je n'aurai jamais ma console[_]!!!";
		attendre une touche;
		fin de l'histoire en disant "Je suis perdu[_]!".

Chapter 1 - Le Wolpertinger

Le Wolpertinger est un animal fixé sur place dans la caverne.
L' indefinite article du Wolpertinger est "le".
La description est "Un effrayant lapin, avec des bois de cerf. Il fiche vraiment la trouille. Qui a eu l'idée de le mettre ici[_]?".

Comprendre "lapin" ou "patte" comme le Wolpertinger.

Before kissing le Wolpertinger pour la première fois:
	dire "Surmontant ma peur, j'embrasse la patte du Wolpertinger. Ça y est, la [italique]chance[romain] est avec moi[_]!";
	augmenter le score de 1;
	arrêter l'action.

Part 8 - Le carrousel

Le carrousel est au sud du stand. "Un antique manège pour enfants. Je venais souvent étant petit. Maintenant, je suis trop grand pour ces chevaux à vilebrequin.[à la ligne]Il y a une boutique à l'est.".
Le printed name est "Devant le carrousel".

Chapter 1 - Le manège

Le manège est une chose décorative dans le carrousel.
La description est "Une vieille attraction, toujours d'actualité : de nombreux enfants viennent toujours profiter de ce manège indémodable. Les chevaux portent même de vrais fers[_]!".

Comprendre "carrousel", "manege", "cheval" ou "chevaux" comme le manège.

Chapter 2 - Le fer à cheval

Le fer est une chose non décrite dans le carrousel.
Le printed name est "fer à cheval".
La description est "Un fer à cheval ordinaire peut-être, mais un fer à cheval porte chance[_]! C'est juste ce qu'il me faut[_]!".

Comprendre "a" ou "cheval" comme le fer.

After taking le fer:
	dire "Profitant de l'arrêt du carrousel et d'un moment d'inattention du forain, j'arrache l'un des fers. Cela porte [italique]chance[romain][_]!";
	augmenter le score de 1.

Instead of dropping le fer when le player porte le fer:
	say "Je ne me séparerai pas d'un porte-bonheur pareil[_]!".

Part 9 - La boutique

La boutique est à l'est du carrousel. "Une petite échoppe tenue par un petit commerçant. Il vend toute sorte de choses. Malheureusement, je garde ma pièce pour la loterie. Peut-être que je pourrais… Non, quand même pas.".
Le printed name est "À l'intérieur d'une petite boutique".

Chapter 1 - Le marchand

Le marchand est un homme non décrit dans la boutique.
La description est "Un petit homme, vendant toutes sortes de choses. Malheureusement, je n'ai pas assez d'argent.".
Le marchand peut être attentif ou distrait. Le marchand est attentif.

Comprendre "homme" comme le marchand.

Instead of talking to le marchand:
	dire "«[_]Alors, quelque chose t'intéresse[_]?[_]»[à la ligne]".

Instead of giving la pièce to le marchand:
	dire "«[_]Désolé, petit. Je n'ai rien à ce prix-là.[_]»[à la ligne]".

Every turn when la location est la boutique:
	si une chance de score sur 6 réussit:
		maintenant le marchand est distrait;
	sinon:
		maintenant le marchand est attentif;
	say "Le marchand [si le marchand est distrait][parmi]regarde derrière lui, cherchant un objet à mettre en vente[ou]est occupé à discuter avec un client[ou]se baisse, cherchant une pièce qu'il vient de faire tomber[au hasard][sinon][parmi]surveille avec attention ses marchandises[ou]est attentif à vos moindres mouvements[ou]n'est pas prêt de se laisser piquer ses affaires[au hasard][fin si].".

Chapter 2 - Le trèfle

Le trèfle est une chose dans la boutique. "Un trèfle est posé sur l'étal.".
La description est "Ce n'est pas n'importe quel trèfle[_]: il s'agit d'un rarissime trèfle à quatre feuilles[_]!".

Comprendre "trefle", "a", "quatre" ou "feuilles" comme le trèfle.

Instead of buying le trèfle:
	si le player porte le trèfle:
		dire "Je n'ai plus besoin de l'acheter maintenant, étant donné que je l'ai… enfin bref.";
	sinon:
		dire "Je n'ai pas assez d'argent.".

Instead of dropping le trèfle when le player porte le trèfle:
	dire "Je ne me séparerai pas d'un porte-bonheur pareil[_]!".

Instead of eating le trèfle:
	say "Je n'allais tout de même pas le manger[_]!".

Taking le trèfle is stealing action.

Carry out taking le trèfle:
	augmenter le score de 1.

Giving le trèfle to le marchand is repentant action.
Showing le trèfle to le marchand is repentant action.

Chapter 3 - La lampe torche

La lampe torche est un appareil éteint dans the boutique. "Une lampe torche est posé sur l'étal.".
La description est "Une puissante lampe torche, capable de dissiper les ténèbres les plus sombres.".

Instead of buying la lampe torche:
	si le player porte la lampe torche:
		dire "Je n'ai plus besoin de l'acheter maintenant, étant donné que je l'ai… enfin bref.";
	sinon:
		dire "Je n'ai pas assez d'argent.".

Instead of dropping la lampe torche when le player porte la lampe torche:
	dire "Je pense que j'en aurai besoin.".

Taking la lampe torche is stealing action.

Giving la lampe torche to le marchand is repentant action.
Showing la lampe torche to le marchand is repentant action.

Chapter 4 - Quand on prend la torche ou le trèfle

After stealing action:
	si le marchand est distrait:
		dire "Je m'approche discrètement de la boutique et subtilise l'objet, sans que le marchand ne me voie[si le noun est le trèfle]. Ce trèfle me portera [italique]chance[romain][fin si].";
	sinon:
		dire "Le marchand m'a vu et me crie dessus. En guise de paiement, il me prend la pièce, même si ce n'est pas suffisant. Je ne pourrais jamais avoir ma console[_]!";
		attendre une touche;
		fin de l'histoire en disant "Je suis perdu[_]!".

Instead of repentant action:
	si le player porte le noun:
		dire "«[_]Mais c'est [le noun] qu'on ma volé[si le noun est female]e[fin si][_]! On se sent coupable, c'est ça[_]?[_]»[à la ligne]En guise de paiement, il me prend la pièce, même si ce n'est pas suffisant. Je n'aurais jamais ma console[_]!";
		attendre une touche;
		fin de l'histoire en disant "Je suis perdu[_]!";
	sinon:
		continue the action.

Volume 3 - Méta
	
Le story headline est "Un loterie interactive".
Le story genre est "Comédie".
Le release number est 2.
Le story description est "Une pièce dans la poche, vous avez décidé d'aller à la loterie afin d'essayer de gagner la toute dernière console de jeux. Cependant, vous connaissez votre chance et il vous sera très difficile de la remporter. À moins que vous trouviez ces objets dont on dit qu'ils portent bonheur...".
Le story creation year est 2011.

Release along with cover art[, a solution, the source text and a file of "Changelog" called "JDC-changelog.txt"].

Book 1 - Les commandes

Part 1 - L'aide

After printing the banner text:
	dire "[première fois]Tapez « aide » pour obtenir plus d'informations sur le jeu ou si vous jouez à une fiction interactive pour la première fois.[seulement]".

Understand "aide", "infos", "info", "informations" or "information" as a mistake ("Ce jeu est une fiction interactive ; il se joue en tapant des commandes à l'infinitif telles que « voir », « prendre [italique]objet[romain] », etc. (pour plus d'informations, consultez [italique]www.ifiction.free.fr[romain]).[à la ligne]Tapez « licence » pour l'obtenir et « auteur » pour en savoir plus sur l'auteur.").

Part 2 - La licence

Understand "licence" or "license" as a mistake ("TODO[paragraph break]Les images de la couverture sont sous la licence Creative Commons BY-SA ([italic type]www.creativecommons.org/licenses/by-sa/3.0/deed.fr[roman type]). Le trèfle est de Phyzome, le fer de Fonzy et le fond de David Monniaux. Les images ont été trouvées sur Wikimedia.").

Part 3 - L'auteur

Understand "credits", "credit", "auteur" or "auteurs" as a mistake ("C'est moi, Natrium729 ! Vous ne savez pas la chance de que vous avez en me connaissant ! Mais si un jour elle vous fait défaut, n'hésitez pas à me contacter à l'adresse [italic type]natrium729@gmail.com[roman type], ou à venir directement commenter sur mon site, [italic type]http://ulukos.com[roman type] ! Au passage, je remercie mes veinard de bêta-testeurs, qui se reconnaîtront (si par hasard vous vous reconnaissez et désirer avoir votre nom d'affiché, dites-le moi, on pourrait s'arranger).").